package de.saviola.irpglp;

import java.util.HashMap;
import java.util.Map;

import de.saviola.irpglp.property.Property;
import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * Represents a hero with unknown properties.
 * 
 * Acts primarily as a storage and accessor for the properties. Thread-safe.
 */
public class Hero
{
  /**
   * Map of properties, the keys being the associated property descriptors.
   */
  private final Map<PropertyDescriptor<?>, Property<?>> properties =
    new HashMap<>();

  /**
   * Creates property on demand.
   */
  private synchronized void createPropertyIfNotExisting(
    final PropertyDescriptor<?> descriptor)
  {
    if (!this.hasProperty(descriptor))
    {
      this.properties
        .put(descriptor, descriptor.createProperty());
    }
  }

  /**
   * Returns the property for the given descriptor.
   */
  public Property<?> getProperty(final PropertyDescriptor<?> descriptor)
  {
    this.createPropertyIfNotExisting(descriptor);

    return this.properties.get(descriptor);
  }

  /**
   * Returns true if the hero has a property for the given property descriptor.
   */
  public boolean hasProperty(final PropertyDescriptor<?> descriptor)
  {
    return this.properties.containsKey(descriptor);
  }
}
