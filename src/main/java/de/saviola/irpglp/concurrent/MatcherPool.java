package de.saviola.irpglp.concurrent;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Matcher pool for processors concurrently performing string matching and
 * not wanting to create a new matcher every time.
 */
public class MatcherPool
{
  /**
   * Number of matchers that will be created.
   */
  private final int matcherCount;

  /**
   * Stack of free matchers. There's currently no way to make sure that
   * matchers are returned at some point, so there might be leaks.
   * 
   * TODO change implementation to avoid leaks and assert unusedness of
   * returned matchers
   */
  Deque<Matcher> free = new ConcurrentLinkedDeque<>();

  /**
   * Creates a new matcher pool with machters for the given pattern.
   */
  public MatcherPool(final Pattern pattern, final int matcherCount)
  {
    this.matcherCount = matcherCount;

    for (int i = 0; i < this.matcherCount; i++)
    {
      this.returnMatcher(pattern.matcher(""));
    }
  }

  /**
   * Returns a free matcher from the stack.
   * 
   * TODO Make sure there are enough matchers
   */
  public Matcher requestMatcher()
  {
    return this.free.remove();
  }

  /**
   * Returns a matcher to the stack.
   * 
   * TODO assert unusedness?
   */
  public void returnMatcher(final Matcher matcher)
  {
    this.free.add(matcher);
  }
}
