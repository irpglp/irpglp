package de.saviola.irpglp;

import java.io.File;

import de.saviola.irpglp.output.TextFileOutputCompiler;
import de.saviola.irpglp.output.TextTableOutput;
import de.saviola.irpglp.processor.IrpglpProcessor;
import de.saviola.irpglp.storage.IrpglpStore;
import de.saviola.irpglp.storage.ProcessorStore;
import de.saviola.regep.core.processing.ConcurrentRecursiveFilteringProcessorChain;
import de.saviola.regep.core.processing.RecursiveFilteringProcessorChain;

/**
 * Main class of the IdleRPG log parser.
 */
public class Irpglp
{
  /**
   * Main method of the IdleRPG log parser.
   *
   * The application will first be initialized, then the log file will be
   * processed before the output will be written.
   */
  public static void main(final String[] args) throws Exception
  {
    // Create the processor chain, 5 threads will be used
    // TODO make number of threads configurable
    final RecursiveFilteringProcessorChain processorChain =
      new ConcurrentRecursiveFilteringProcessorChain(5);

    // Create and initialize the main storage
    final IrpglpStore storage = new IrpglpStore();
    storage.initialize();

    // Add all processors to the processor chain
    for (final IrpglpProcessor processor : ((ProcessorStore) storage
      .get("processor"))
      .getAll())
    {
      processorChain.addProcessor(processor);
    }

    // Process the log file
    // TODO support multiple files and extract this into the configuration
    final File file =
      args.length > 0 ? new File(args[0]) : new File("idle.log");
    processorChain.process(file);

    // Create and configure the output creator
    // TODO extract to configuration
    final TextTableOutput output = new TextTableOutput(storage,
      new String[]
      { "name", "level", "equipment", "fights", "fate",
        "name", "alignment", "quest", "meta",
      });

    // Write the different parts of the output
    // TODO extract paths to configuration
    // TODO create directory
    output.write(new File("/tmp/idle/table.txt"));
    output.writeColumnDescriptions(new File("/tmp/idle/table_description.txt"));

    // Create output compiler and pass the output file
    // TODO extract path to configuration
    final TextFileOutputCompiler outputCompiler =
      new TextFileOutputCompiler(new File("/tmp/idle/output.txt"));

    // Compile the output and save it to the above file
    outputCompiler.compileAndSave();
  }
}
