package de.saviola.irpglp.property;

import java.util.concurrent.atomic.AtomicInteger;

import de.saviola.irpglp.storage.CounterStore;
import de.saviola.irpglp.storage.interfaces.SimpleStore;

/**
 * {@link Property} which provides a {@link CounterStore}.
 */
public abstract class CounterStoreProperty<P extends Property<P>> extends
  Property<P>
{
  /**
   * {@link CounterStore} of this property.
   */
  private final SimpleStore<AtomicInteger> counterStore =
    new CounterStore();

  /**
   * Returns the counter with the given ID.
   */
  public AtomicInteger getCounter(final String counterId)
  {
    return this.counterStore.get(counterId);
  }

  /**
   * Returns the counter store.
   */
  public SimpleStore<AtomicInteger> getCounterStore()
  {
    return this.counterStore;
  }
}
