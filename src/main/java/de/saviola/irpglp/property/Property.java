package de.saviola.irpglp.property;

import de.saviola.irpglp.model.LineNumberTracker;

/**
 * Represents a generic property.
 * 
 * A property consists of a {@link PropertyDescriptor} and must return a string
 * representation via {@link #toString()}.
 * 
 * @param <P> Property to reference in the property descriptor
 * 
 * TODO remove recursive generic type?
 */
abstract public class Property<P extends Property<P>>
{
  /**
   * Line number tracker which may be used by the property.
   */
  private final LineNumberTracker lineNumberTracker = new LineNumberTracker();

  /**
   * The property descriptor for this property.
   */
  private PropertyDescriptor<P> descriptor;

  /**
   * Protected constructor to avoid direct property creation.
   */
  protected Property()
  {
  }

  /**
   * Returns the associated property descriptor.
   */
  public PropertyDescriptor<P> getDescriptor()
  {
    return this.descriptor;
  }

  /**
   * Returns the line number tracker of this property.
   */
  public LineNumberTracker getLineNumberTracker()
  {
    return this.lineNumberTracker;
  }

  /**
   * Returns an array of column values of this property.
   * 
   * @see PropertyDescriptor#getColumns()
   */
  abstract public String[] getValues();

  /**
   * Sets the associated property descriptor.
   */
  public void setDescriptor(final PropertyDescriptor<P> descriptor)
  {
    this.descriptor = descriptor;
  }
}
