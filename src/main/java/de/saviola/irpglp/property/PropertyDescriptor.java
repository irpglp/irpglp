package de.saviola.irpglp.property;

/**
 * Represents an abstract descriptor for properties, consisting of an id and a
 * description.
 * 
 * Properties musst only be created using {@link #createProperty()}.
 * 
 * @param <P> Property that is being described.
 */
abstract public class PropertyDescriptor<P extends Property<P>>
{
  /**
   * Property descriptor ID.
   */
  private final String id;

  /**
   * Creates a property descriptor using an id and a description.
   * 
   * To be used from concrete descriptors.
   */
  public PropertyDescriptor(final String id)
  {
    this.id = id;
  }

  /**
   * Creates a property, generics are used to avoid having to cast.
   */
  abstract public P createProperty();

  /**
   * Returns the columns this property is going to create.
   * 
   * Expected form:
   * 
   * new String[] {
   *  "description column1", "l1", "a1", "column1 description"
   *  "description column2", "l2", "a2", "column2 description"
   *  …
   * }
   * 
   * with l1…n being the column length (0 for automatic) and a1…n being the
   * alignment, which is expected to be one of "l" (left)", "c" (center) and
   * "r" (right). 
   */
  abstract public String[] getColumns();

  /**
   * Returns the property ID.
   */
  public String getId()
  {
    return this.id;
  }

  /**
   * Returns the string representation of this descriptor, i.e. its ID.
   */
  @Override
  public String toString()
  {
    return this.getId();
  }
}
