package de.saviola.irpglp.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.concurrent.MatcherPool;
import de.saviola.irpglp.storage.PatternStore;

/**
 * Represents a processor which processes {@link String} objects with a certain
 * patternId.
 * 
 * TODO update documentation
 */
public abstract class PatternMatchingStringProcessor extends
  IrpglpProcessor
{
  private final static Logger logger = LoggerFactory
    .getLogger(PatternMatchingStringProcessor.class);

  /**
   * Pattern against which {@link String}s are matched.
   */
  private Pattern pattern;

  /**
   * ID of the pattern.
   * 
   * @see #pattern
   */
  private final String patternId;

  /**
   * {@link MatcherPool} to allow for parallel processing using a single
   * processor instance.
   */
  private MatcherPool matcherPool;

  /**
   * Creates a {@link PatternMatchingStringProcessor} for the given pattern ID.
   */
  public PatternMatchingStringProcessor(final String patternId)
  {
    this.patternId = patternId;
  }

  /**
   * Debug method.
   * 
   * TODO move to unit tests
   */
  protected void debugInput(final Object input)
  {
    // Request a free matcher from the pool
    final Matcher matcher = this.getMatcherPool().requestMatcher();

    // Reset it with the given input
    matcher.reset((String) input).find();

    for (int i = 0; i <= matcher.groupCount(); i++)
    {
      logger.debug("Group {}: \"{}\"", matcher.group(i));
    }

    // Return the matcher to the pool
    this.getMatcherPool().returnMatcher(matcher);
  }

  /**
   * Convenience method to extract the hero name from the given input.
   * 
   * The hero name is expected to be matched by a named group "hero".
   * 
   * TODO rename to extractHeroName?
   */
  protected Hero extractHero(final Object input)
  {
    return this.extractHeroes(input, new String[] { "hero" }).get(0);
  }

  /**
   * Extracts heroes' names from the given input.
   * 
   * The second argument contains an array of {@link String}s representing the
   * named groups by which the heroes' names are matched. 
   * 
   * TODO rename to extractHeroesNames?
   */
  protected List<Hero> extractHeroes(final Object input,
    final String[] patternIdentifiers)
  {
    // Extract named groups
    final List<String> groups = this.getNamedGroups(input.toString(),
      patternIdentifiers);

    final List<Hero> heroes = new ArrayList<>();

    // Extract heroes' names
    for (int i = 0; i < patternIdentifiers.length; i++)
    {
      heroes.add((Hero) this.getStorage().get("hero")
        .get(groups.get(i)));
    }

    return heroes;
  }

  /**
   * Returns the matcher pool.
   */
  public synchronized MatcherPool getMatcherPool()
  {
    // Creates a new matcher pool if it doesn't exist
    if (this.matcherPool == null)
    {
      this.matcherPool = new MatcherPool(this.getPattern(), 25);
    }

    return this.matcherPool;
  }

  /**
   * Returns the input substring captured by the given named groups in the
   * given input string.
   * 
   * TODO rename for clarity?
   */
  public List<String> getNamedGroups(final Object input, final String[] groups)
  {
    final List<String> groupList = new ArrayList<>();

    // Request a free matcher from the pool
    final Matcher matcher = this.getMatcherPool().requestMatcher();

    // Reset it with the given input
    matcher.reset((String) input).find();

    // Walk through the groups and add the captured substring
    for (final String group : groups)
    {
      groupList.add(matcher.group(group));
    }

    // Return the matcher to the pool
    this.getMatcherPool().returnMatcher(matcher);

    return groupList;
  }

  /**
   * Returns the pattern of this processor.
   */
  public Pattern getPattern()
  {
    if (this.pattern == null)
    {
      this.pattern = this.getPatternStorage().get(this.getPatternId());

      if (this.pattern == null)
      {
        throw new RuntimeException("Pattern not found: " + this.getPatternId());
      }
    }

    return this.pattern;
  }

  /**
   * Returns the ID of the pattern of this processor.
   */
  public String getPatternId()
  {
    return this.patternId;
  }

  /**
   * Returns the pattern storage.
   */
  public final PatternStore getPatternStorage()
  {
    return (PatternStore) this.getStorage().get("pattern");
  }

  /**
   * Returns an array of regular expressions which are then compiled into
   * patterns.
   * 
   * In the array, each regular expression is preceded by its identifier:
   * 
   * new String[] { "nameOfPattern1", "actualPattern1", … }
   */
  abstract public String[] getRegularExpressions();

  /**
   * Returns whether the given input object is a string which matches the
   * patternId.
   */
  @Override
  public boolean isApplicableFor(final Object input)
  {
    // Check if it's a string
    if (!(input instanceof String))
    {
      return false;
    }

    // Request a free matcher from the pool
    final Matcher matcher = this.getMatcherPool().requestMatcher();

    // Reset it with the given input
    matcher.reset((String) input);

    // Match the pattern
    final boolean found = matcher.find();

    // Return the matcher to the pool
    this.getMatcherPool().returnMatcher(matcher);

    return found;
  }
}
