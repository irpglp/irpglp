package de.saviola.irpglp.processor;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.property.Property;
import de.saviola.irpglp.property.PropertyDescriptor;
import de.saviola.irpglp.storage.IrpglpStore;
import de.saviola.irpglp.storage.interfaces.SimpleStore;
import de.saviola.regep.core.processing.AbstractProcessor;

/**
 * Abstract processor providing access to the store.
 */
public abstract class IrpglpProcessor extends AbstractProcessor
{
  /**
   * {@link IrpglpStore} containing application data.
   */
  private SimpleStore<SimpleStore<?>> store;

  /**
   * Convenience mehtod which returns the given hero's property for the given
   * property ID.
   */
  public Property<?> getProperty(final Hero hero, final String propertyId)
  {
    return hero.getProperty((PropertyDescriptor<?>)
      this.getStorage().get("propertyDescriptor").get(propertyId));
  }

  /**
   * Returns the main store.
   */
  public SimpleStore<SimpleStore<?>> getStorage()
  {
    return this.store;
  }

  /**
   * Sets the main store.
   */
  public void setStorage(final SimpleStore<SimpleStore<?>> simpleStore)
  {
    this.store = simpleStore;
  }
}
