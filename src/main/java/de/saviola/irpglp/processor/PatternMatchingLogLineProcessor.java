package de.saviola.irpglp.processor;

import java.util.List;

import de.saviola.irpglp.modules.log.model.LogLine;

/**
 * Pattern matching processor which processes log lines instead of strings.
 * 
 * Extends and uses the {@link PatternMatchingStringProcessor}.
 * 
 * Overridden methods provide automatic string conversion for the input object.
 */
public abstract class PatternMatchingLogLineProcessor extends
  PatternMatchingStringProcessor
{
  public PatternMatchingLogLineProcessor(final String patternId)
  {
    super(patternId);
  }

  @Override
  public List<String> getNamedGroups(final Object input, final String[] groups)
  {
    final String inputString =
      input instanceof String ? (String) input : input.toString();

    return super.getNamedGroups(inputString, groups);
  }

  @Override
  public boolean isApplicableFor(final Object input)
  {
    return input instanceof LogLine &&
      super.isApplicableFor(input.toString());
  }
}
