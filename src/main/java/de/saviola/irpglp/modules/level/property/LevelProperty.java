package de.saviola.irpglp.modules.level.property;

import de.saviola.irpglp.property.Property;

/**
 * {@link Property} for the hero's level.
 * 
 * Additionally, the time to the next level is stored.
 */
public class LevelProperty extends Property<LevelProperty>
{
  /**
   * The hero's level.
   */
  private int level;

  /**
   * The time until the hero reaches the next level.
   */
  private long secondsToNext;

  /**
   * Returns the hero's level.
   */
  public synchronized int getLevel()
  {
    return this.level;
  }

  /**
   * Returns the time to the next level.
   */
  public synchronized long getSecondsToNext()
  {
    return this.secondsToNext;
  }

  @Override
  public String[] getValues()
  {
    long seconds = this.getSecondsToNext();
    final int days = (int) (seconds / 60 / 60 / 24);
    seconds -= days * 24 * 60 * 60;
    final int hours = (int) (seconds / 60 / 60);
    seconds -= hours * 60 * 60;
    final int minutes = (int) (seconds / 60);
    seconds -= minutes * 60;

    return new String[]
    {
      String.format("%d", this.getLevel()),
      String.format("%dd, %02dh:%02d:%02d",
        days, hours, minutes, (int) seconds),
    };
  }

  /**
   * Sets the hero's level.
   * 
   * The level can only increase and never decrease (i.e., lower values than
   * the current level will be ignored).
   */
  public synchronized void setLevel(final int level)
  {
    final int currentLevel = this.getLevel();

    this.level = level >= currentLevel ? level : currentLevel;
  }

  /**
   * Sets the time to the next level.
   */
  public synchronized void setSecondsToNext(final long secondsToNext,
    final long lineNumber)
  {
    if (this.getLineNumberTracker().checkAndUpdateLineNumber(lineNumber))
    {
      this.secondsToNext = secondsToNext;
    }
  }

}
