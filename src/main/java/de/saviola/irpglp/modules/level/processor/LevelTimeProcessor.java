package de.saviola.irpglp.modules.level.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.level.property.LevelProperty;
import de.saviola.irpglp.modules.log.model.LogLine;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name> reaches the next level in <time>"
 */
public class LevelTimeProcessor extends PatternMatchingLogLineProcessor
{
  public LevelTimeProcessor()
  {
    super("toNextLevel");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "idleTime", "(?<days>\\d+) days, (?<time>$(timestamp))",
      "toNextLevel", "(?<name>$(name)) reaches next level in $(idleTime).",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final List<String> groups =
      this.getNamedGroups(input.toString(), new String[] {
        "days", "time",
      });

    final Hero hero =
      this.extractHeroes(input, new String[] { "name", }).get(0);
    final LevelProperty level = (LevelProperty) this.getProperty(hero, "level");

    // Update the seconds to the next level
    level.setSecondsToNext(
      Integer.valueOf(groups.get(0)).intValue() * 24 * 60 * 60
        + Integer.valueOf(groups.get(1).substring(0, 2)).intValue() * 60 * 60
        + Integer.valueOf(groups.get(1).substring(3, 5)).intValue() * 60
        + Integer.valueOf(groups.get(1).substring(6, 8)).intValue(),
      ((LogLine) input).getLineNumber());

    return Collections.emptyList();
  }
}
