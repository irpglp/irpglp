package de.saviola.irpglp.modules.level.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link LevelProperty}.
 */
public class LevelDescriptor extends PropertyDescriptor<LevelProperty>
{
  public LevelDescriptor()
  {
    super("level");
  }

  @Override
  public LevelProperty createProperty()
  {
    return new LevelProperty();
  }

  @Override
  public String[] getColumns()
  {
    return new String[] {
      "level", "0", "r", "level of the hero",
      "to next", "15", "r", "current time to the next level",
    };
  }

}
