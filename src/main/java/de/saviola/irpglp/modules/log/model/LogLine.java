package de.saviola.irpglp.modules.log.model;

import de.saviola.irpglp.model.Model;

/**
 * A log line representation consisting of the line and line number.
 */
public class LogLine implements Model
{
  /**
   * The represented line.
   */
  private final String line;

  /**
   * Line number of this line in the context of, e.g., its log file.
   */
  private final Long lineNumber;

  public LogLine(final String line, final Long lineNumber)
  {
    this.line = line;
    this.lineNumber = lineNumber;
  }

  /**
   * Returns the line.
   */
  public String getLine()
  {
    return this.line;
  }

  /**
   * Returns the line number.
   */
  public long getLineNumber()
  {
    return this.lineNumber.longValue();
  }

  @Override
  public String toString()
  {
    return this.line;
  }
}
