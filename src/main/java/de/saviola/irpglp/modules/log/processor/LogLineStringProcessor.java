package de.saviola.irpglp.modules.log.processor;

import java.util.Collection;
import java.util.List;

import de.saviola.irpglp.model.Model;
import de.saviola.irpglp.modules.log.model.LogLine;
import de.saviola.irpglp.processor.PatternMatchingStringProcessor;

/**
 * Processor that cleans log lines posted by the IdleRPG bot of timestamps and
 * other unnecessary stuff.
 */
public class LogLineStringProcessor extends PatternMatchingStringProcessor
{
  public LogLineStringProcessor()
  {
    super("logLine");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "lineCount", "LC#(?<lineCount>\\d+)# ",
      "timestamp", "\\d\\d:\\d\\d:\\d\\d",
      "bncTimestamp", "\\[$(timestamp)\\]",
      "name", "[^ ,]+",
      "equipment", "\\[\\d+/\\d+\\]",
      "logLine", "(?<linePrefix>$(lineCount)$(timestamp)<@IdleRPG> " +
        "(?:$(bncTimestamp) )?)",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final List<String> groups = this.getNamedGroups(input,
      new String[] { "linePrefix", "lineCount" });

    final Model event =
      new LogLine(((String) input).replace(groups.get(0), ""),
        Long.valueOf(groups.get(1)));

    return this
      .collectionFromObject(event);
  }
}
