package de.saviola.irpglp.modules.fight.processor.logline;

import java.util.Collection;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.equipment.property.EquipmentProperty;
import de.saviola.irpglp.modules.fight.model.Fight;
import de.saviola.irpglp.modules.log.model.LogLine;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name> <equipment> has challenged/come upon <name> <equipment> (in combat)
 * and won/lost!"
 */
public class SingleFightProcessor extends
  PatternMatchingLogLineProcessor
{
  public SingleFightProcessor()
  {
    super("singleFight");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "singleFightChallenger", "(?<nameChallenger>$(name)) " +
        "(?<equipmentChallenger>$(equipment))",
      "singleFightChallenged", "(?<nameChallenged>$(name)) " +
        "(?<equipmentChallenged>$(equipment))",
      "singleFightBegin", "challenged|come upon",
      "singleFightEnd", "(?:in combat )?and",
      "singleFightEndWin", "taken them in combat|won",
      "singleFightEndLoss", "been defeated in combat|lost",
      "singleFightResultKeyword", "(?<resultKeyword>added|removed)",
      "singleFight", "$(singleFightChallenger) has " +
        "(?:$(singleFightBegin)) $(singleFightChallenged) $(singleFightEnd) " +
        "(?:$(singleFightEndWin)|$(singleFightEndLoss))!.*" +
        "$(singleFightResultKeyword)",
    };
  }

  /**
   * Processes the log line.
   * 
   * The line is matched an all important information stored in a
   * {@link Fight} object, which is then returned.
   */
  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    // Extract the fighters
    final List<Hero> fighters = this.extractHeroes(input,
      new String[] { "nameChallenger", "nameChallenged", });

    // Extract the keyword signalling which hero won
    final List<String> groups = this.getNamedGroups(input.toString(),
      new String[] { "resultKeyword", });

    final List<String> equipment = this.getNamedGroups(input.toString(),
      new String[] { "equipmentChallenger", "equipmentChallenged" });

    final long lineNumber = ((LogLine) input).getLineNumber();

    // Update the equipment of involved heroes
    for (int i = 0; i < fighters.size(); i++)
    {
      this.registerEquipment(fighters.get(i), equipment.get(i), lineNumber);
    }

    final boolean challengersWin =
      groups.get(0).equals("removed") ? true : false;

    // Create the fight object and delegate processing to the FightProcessor
    final Fight fight = new Fight(fighters, challengersWin);

    return super.collectionFromObject(fight);
  }

  /**
   * Helper method to register the equipment value of a hero.
   */
  private void registerEquipment(final Hero hero, final String equipment,
    final long lineNumber)
  {
    // Extract the equipment value from the string:
    // [currentEquipment/overallEquipment]
    final int equipmentValue =
      Integer.valueOf(equipment.substring(equipment.indexOf("/") + 1,
        equipment.indexOf("]"))).intValue();

    // Update the equipment value
    ((EquipmentProperty) this.getProperty(hero, "equipment"))
      .setEquipmentValue(equipmentValue, lineNumber);
  }
}
