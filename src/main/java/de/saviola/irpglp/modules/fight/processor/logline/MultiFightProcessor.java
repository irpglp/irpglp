package de.saviola.irpglp.modules.fight.processor.logline;

import java.util.Collection;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.fight.model.Fight;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name>, <name> and <name> <equipment> have team battled <name>, <name> and
 * <name> <equipment> and won/lost!"
 */
public class MultiFightProcessor extends PatternMatchingLogLineProcessor
{
  public MultiFightProcessor()
  {
    super("multiFight");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "multiFightChallengers", "(?<nameChallenger1>$(name)), " +
        "(?<nameChallenger2>$(name)), and (?<nameChallenger3>$(name)) " +
        "$(equipment)",
      "multiFightBegin", "have team battled",
      "multiFightChallenged", "(?<nameChallenged1>$(name)), " +
        "(?<nameChallenged2>$(name)), and (?<nameChallenged3>$(name)) " +
        "$(equipment)",
      "multiFightResultKeyword", "(?<resultKeyword>won|lost)",
      "multiFight", "$(multiFightChallengers) $(multiFightBegin) " +
        "$(multiFightChallenged) and $(multiFightResultKeyword)!",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final List<Hero> fighters = this.extractHeroes(input,
      new String[] { "nameChallenger1", "nameChallenger2", "nameChallenger3",
        "nameChallenged1", "nameChallenged2", "nameChallenged3",
      });

    final List<String> groups =
      this.getNamedGroups(input.toString(), new String[] { "resultKeyword", });

    final boolean challengersWin =
      groups.get(0).equals("won") ? true : false;

    // Create fight object and delegate processing to the FightProcessor
    final Fight fight = new Fight(fighters, challengersWin);

    return super.collectionFromObject(fight);
  }
}
