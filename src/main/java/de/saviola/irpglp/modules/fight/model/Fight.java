package de.saviola.irpglp.modules.fight.model;

import java.util.ArrayList;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.model.HeroModel;

/**
 * Represents a fight between 2 (1v1) or 6 (3v3) fighters.
 */
public class Fight implements HeroModel
{
  /**
   * Involved fighters. By convention, the number of fighters is always even
   * and the first half consists of challengers while the second half consists
   * of challenged figters.
   */
  private final List<Hero> fighters;

  /**
   * Whether the challengers won this fight.
   */
  private final boolean challengersWin;

  /**
   * Creates a new fight event with the given figthers and outcome.
   */
  public Fight(final List<Hero> fighters, final boolean challengersWin)
  {
    this.fighters = fighters;
    this.challengersWin = challengersWin;
  }

  /**
   * Returns a list of challenged fighters.
   */
  public List<Hero> getChallenged()
  {
    return this.getFighters(this.fighters.size() / 2, this.fighters.size());
  }

  /**
   * Returns a list of challenging fighters.
   */
  public List<Hero> getChallengers()
  {
    return this.getFighters(0, this.fighters.size() / 2);
  }

  private List<Hero> getFighters(final int startIndex, final int endIndex)
  {
    final List<Hero> fighters = new ArrayList<>();

    for (int i = startIndex; i < endIndex; i++)
    {
      fighters.add(this.fighters.get(i));
    }

    return fighters;
  }

  /**
   * Returns all involved heroes.
   * 
   * @see #fighters
   */
  @Override
  public List<Hero> getInvolvedHeroes()
  {
    return this.fighters;
  }

  /**
   * Returns true if the challenging fighters have won.
   */
  public boolean isWin()
  {
    return this.challengersWin;
  }
}
