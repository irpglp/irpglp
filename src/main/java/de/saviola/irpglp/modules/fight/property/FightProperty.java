package de.saviola.irpglp.modules.fight.property;

import de.saviola.irpglp.property.CounterStoreProperty;

/**
 * {@link CounterStoreProperty} for fights.
 * 
 * The number of fights and the number of fights won are stored.
 */
public class FightProperty extends CounterStoreProperty<FightProperty>
{
  @Override
  public String[] getValues()
  {
    final int numberOfFights = this.getCounter("numberOfFights").get();
    final int numberOfFightsWon = this.getCounter("numberOfFightsWon").get();

    return new String[]
    {
      String.format("%d", numberOfFights),
      String.format("%d", numberOfFightsWon),
      String.format("%.2f%%",
        100. * numberOfFightsWon / numberOfFights),
    };
  }
}
