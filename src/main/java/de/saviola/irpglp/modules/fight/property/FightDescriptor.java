package de.saviola.irpglp.modules.fight.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link FightProperty}.
 */
public class FightDescriptor extends PropertyDescriptor<FightProperty>
{
  public FightDescriptor()
  {
    super("fights");
  }

  @Override
  public FightProperty createProperty()
  {
    return new FightProperty();
  }

  @Override
  public String[] getColumns()
  {
    return new String[]
    {
      "fights", "0", "r",
      "number of fights in which the hero was involved",
      "won", "4", "r",
      "number of fights the hero has won",
      "%won", "7", "r",
      "fight win percentage of the hero",
    };
  }
}
