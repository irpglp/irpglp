package de.saviola.irpglp.modules.fight.processor;

import java.util.Collection;
import java.util.Collections;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.fight.model.Fight;
import de.saviola.irpglp.modules.fight.property.FightProperty;
import de.saviola.irpglp.processor.IrpglpProcessor;

/**
 * Processor of {@link Fight}s.
 */
public class FightProcessor extends IrpglpProcessor
{
  /**
   * Returns true if the input object is a {@link Fight}.
   */
  @Override
  public boolean isApplicableFor(final Object input)
  {
    return input instanceof Fight;
  }

  /**
   * Processes the given {@link Fight}.
   * 
   * Basically, wins and losses are recorded in the {@link Hero}es' properies.
   */
  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final Fight event = (Fight) input;

    // Walk through all involved heroes and increment their fight counter
    for (final Hero fighter : event.getInvolvedHeroes())
    {
      ((FightProperty) this.getProperty(fighter, "fights"))
        .getCounter("numberOfFights").incrementAndGet();
    }

    // Walk through all winners and increment their win counter
    for (final Hero winner : event.isWin() ? event.getChallengers() : event
      .getChallenged())
    {
      ((FightProperty) this.getProperty(winner, "fights"))
        .getCounter("numberOfFightsWon").incrementAndGet();
    }

    return Collections.emptyList();
  }
}
