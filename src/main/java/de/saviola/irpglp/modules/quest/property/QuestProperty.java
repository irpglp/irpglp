package de.saviola.irpglp.modules.quest.property;

import de.saviola.irpglp.property.CounterStoreProperty;

/**
 * {@link CounterStoreProperty} for quest counters.
 * 
 * Stored counter are:
 * 
 * - number of quests started
 * - number of quests comleted
 * - number of quests failed
 */
public class QuestProperty extends CounterStoreProperty<QuestProperty>
{
  @Override
  public String[] getValues()
  {
    return new String[]
    {
      String.format("%d", this.getCounter("questsStarted").get()),
      String.format("%d", this.getCounter("questsCompleted").get()),
      String.format("%d", this.getCounter("questsFailed").get()),
    };
  }
}
