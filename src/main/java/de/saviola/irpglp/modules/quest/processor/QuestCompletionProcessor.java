package de.saviola.irpglp.modules.quest.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.quest.property.QuestProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name>, <name> and <name> have completed their journey![...]"
 */
public class QuestCompletionProcessor extends PatternMatchingLogLineProcessor
{
  public QuestCompletionProcessor()
  {
    super("questCompletion");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[]
    {
      "questCompletion", "(?<h1>$(name)), (?<h2>$(name)), (?<h3>$(name)), " +
        "and (?<h4>$(name)) have completed their journey!.*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final List<Hero> heroes = this.extractHeroes(input,
      new String[] { "h1", "h2", "h3", "h4", });

    // Increment completed quests for all involved heroes
    for (final Hero hero : heroes)
    {
      ((QuestProperty) this.getProperty(hero, "quest"))
        .getCounter("questsCompleted").incrementAndGet();
    }

    return Collections.emptyList();
  }
}
