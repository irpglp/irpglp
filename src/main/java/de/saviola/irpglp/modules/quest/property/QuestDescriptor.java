package de.saviola.irpglp.modules.quest.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link QuestProperty}.
 */
public class QuestDescriptor extends PropertyDescriptor<QuestProperty>
{
  public QuestDescriptor()
  {
    super("quest");
  }

  @Override
  public QuestProperty createProperty()
  {
    return new QuestProperty();
  }

  @Override
  public String[] getColumns()
  {
    return new String[]
    {
      "quests", "0", "r", "number of quests the hero has been involved in",
      "completed", "0", "r", "number of completed quests",
      "failed", "0", "r",
      "number of quests the hero has caused to fail (see below)",
    };
  }
}
