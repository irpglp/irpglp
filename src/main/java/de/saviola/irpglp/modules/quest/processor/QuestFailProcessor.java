package de.saviola.irpglp.modules.quest.processor;

import java.util.Collection;
import java.util.Collections;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.quest.property.QuestProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name>'s prudence and self-regard[...]"
 */
public class QuestFailProcessor extends PatternMatchingLogLineProcessor
{
  public QuestFailProcessor()
  {
    super("questFail");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[]
    {
      "questFail", "(?<hero>$(name))'s prudence and self-regard.*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final Hero hero = this.extractHeroes(input, new String[] { "hero" }).get(0);

    ((QuestProperty) this.getProperty(hero, "quest"))
      .getCounter("questsFailed").incrementAndGet();

    return Collections.emptyList();
  }
}
