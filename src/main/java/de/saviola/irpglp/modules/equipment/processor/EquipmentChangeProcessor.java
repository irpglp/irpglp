package de.saviola.irpglp.modules.equipment.processor;

import java.util.Collection;
import java.util.Collections;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.equipment.property.EquipmentProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "[...]! <name>'s gains/loses 10% (of its )effectiveness."
 */
public class EquipmentChangeProcessor extends PatternMatchingLogLineProcessor
{
  public EquipmentChangeProcessor()
  {
    super("equipmentChange");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[]
    {
      "equipmentChange", ".*! (?<hero>$(name))'s.*" +
        "(?<equipmentChange>gains|loses) 10% (?:of its )?effectiveness.",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final Hero hero = this.extractHeroes(input, new String[] { "hero" }).get(0);

    final String equipmentChange =
      this.getNamedGroups(input, new String[] { "equipmentChange" }).get(0);
    final boolean equipmentImproved =
      equipmentChange.equals("gains") ? true : false;

    final EquipmentProperty equipmentProperty =
      (EquipmentProperty) this.getProperty(hero, "equipment");

    if (equipmentImproved)
    {
      equipmentProperty.getCounter("equipmentImprovements").incrementAndGet();
    }
    else
    {
      equipmentProperty.getCounter("equipmentDegradations").incrementAndGet();
    }

    return Collections.emptyList();
  }
}
