package de.saviola.irpglp.modules.equipment.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link EquipmentProperty}.
 */
public class EquipmentDescriptor extends PropertyDescriptor<EquipmentProperty>
{
  public EquipmentDescriptor()
  {
    super("equipment");
  }

  @Override
  public EquipmentProperty createProperty()
  {
    return new EquipmentProperty();
  }

  @Override
  public String[] getColumns()
  {
    return new String[]
    {
      "equipment", "0", "r",
      "sum of the equipped items' values. Value as used in fights, not " +
        "the base value (alignment affects this)",
      "+10", "0", "r",
      "number of times an item's value was increased by 10%",
      "-10", "0", "r",
      "likewise, the number of times an item's value was decreased by 10%",
      "droppped", "0", "r",
      "number of times an item was dropped during a fight",
      "taken", "0", "r",
      "number of times an item was taken from an opponent during a fight",
    };
  }

}
