package de.saviola.irpglp.modules.equipment.property;

import de.saviola.irpglp.property.CounterStoreProperty;

/**
 * {@link CounterStoreProperty} for the hero's equipment.
 * 
 * In addition to the equipment value, four counters are stored here:
 * 
 * - the number of equipment improvements
 * - the number of equipment degradations
 * - the number of items dropped
 * - the number of items taken
 */
public class EquipmentProperty extends CounterStoreProperty<EquipmentProperty>
{
  /**
   * Equipment value of the hero.
   */
  private int equipmentValue = 0;

  /**
   * Returns the equipment value.
   */
  public int getEquipmentValue()
  {
    return this.equipmentValue;
  }

  @Override
  public String[] getValues()
  {
    return new String[]
    {
      String.format("%d", this.getEquipmentValue()),
      String.format("%d", this.getCounter("equipmentImprovements").get()),
      String.format("%d", this.getCounter("equipmentDegradations").get()),
      String.format("%d", this.getCounter("itemsDropped").get()),
      String.format("%d", this.getCounter("itemsTaken").get()),
    };
  }

  /**
   * Sets the equipment value if no newer value is known.
   */
  public synchronized void setEquipmentValue(final int equipmentValue,
    final long lineNumber)
  {
    // Only update the equipment value if it is the newest known
    if (this.getLineNumberTracker().checkAndUpdateLineNumber(lineNumber))
    {
      this.equipmentValue = equipmentValue;
    }
  }
}
