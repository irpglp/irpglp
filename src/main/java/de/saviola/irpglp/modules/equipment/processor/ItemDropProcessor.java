package de.saviola.irpglp.modules.equipment.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.equipment.property.EquipmentProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "In the fierce battle, <name> dropped his level[...]! <name> picks it up,
 * tossing his old level[...]"
 */
public class ItemDropProcessor extends PatternMatchingLogLineProcessor
{
  public ItemDropProcessor()
  {
    super("itemDrop");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[]
    {
      "itemDrop", "In the fierce battle, (?<heroDrop>$(name)) dropped his " +
        "level.*! (?<heroTake>$(name)) picks it up, tossing his old level.*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final List<Hero> heroes = this.extractHeroes(input,
      new String[] { "heroDrop", "heroTake" });

    // Update heroes' properties
    ((EquipmentProperty) this.getProperty(heroes.get(0), "equipment"))
      .getCounter("itemsDropped").incrementAndGet();
    ((EquipmentProperty) this.getProperty(heroes.get(1), "equipment"))
      .getCounter("itemsTaken").incrementAndGet();

    return Collections.emptyList();
  }
}
