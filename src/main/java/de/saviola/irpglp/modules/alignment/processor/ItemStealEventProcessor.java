package de.saviola.irpglp.modules.alignment.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.alignment.property.AlignmentProperty;
import de.saviola.irpglp.modules.alignment.property.AlignmentProperty.Alignment;
import de.saviola.irpglp.modules.log.model.LogLine;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;
import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name> stole <name>'s level[...]"
 */
public class ItemStealEventProcessor extends PatternMatchingLogLineProcessor
{
  public ItemStealEventProcessor()
  {
    super("steal");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "steal", "(?<nameThief>$(name)) stole (?<nameVictim>$(name))'s level.*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final List<Hero> heroes = this.extractHeroes(input,
      new String[] { "nameThief", "nameVictim", });

    final long lineNumber = ((LogLine) input).getLineNumber();

    // Update properties
    final AlignmentProperty thiefAlignmentProperty =
      (AlignmentProperty) heroes.get(0).getProperty((PropertyDescriptor<?>)
        this.getStorage().get("propertyDescriptor").get("alignment"));
    thiefAlignmentProperty.getCounter("itemsStolenThief").incrementAndGet();
    thiefAlignmentProperty.setAlignment(Alignment.EVIL, lineNumber);

    final AlignmentProperty victimAlignmentProperty =
      (AlignmentProperty) heroes.get(1).getProperty((PropertyDescriptor<?>)
        this.getStorage().get("propertyDescriptor").get("alignment"));
    victimAlignmentProperty.getCounter("itemsStolenVictim").incrementAndGet();
    victimAlignmentProperty.setAlignment(Alignment.GOOD, lineNumber);

    return Collections.emptyList();
  }
}
