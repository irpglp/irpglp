package de.saviola.irpglp.modules.alignment.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.alignment.property.AlignmentProperty;
import de.saviola.irpglp.modules.alignment.property.AlignmentProperty.Alignment;
import de.saviola.irpglp.modules.log.model.LogLine;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name> and <name> have not let the iniquities[...]"
 */
public class PrayerProcessor extends PatternMatchingLogLineProcessor
{
  public PrayerProcessor()
  {
    super("prayer");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "prayer", "(?<hero1>$(name)) and (?<hero2>$(name)) have not let the " +
        "iniquities.*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    // Extract heroes
    final List<Hero> heroes = this.extractHeroes(input,
      new String[] { "hero1", "hero2", });

    final long lineNumber = ((LogLine) input).getLineNumber();

    // Update properties
    final AlignmentProperty hero1AlignmentProperty =
      (AlignmentProperty) this.getProperty(heroes.get(0), "alignment");
    hero1AlignmentProperty.getCounter("prayers").incrementAndGet();
    hero1AlignmentProperty.setAlignment(Alignment.GOOD, lineNumber);

    final AlignmentProperty hero2AlignmentProperty =
      (AlignmentProperty) this.getProperty(heroes.get(1), "alignment");
    hero2AlignmentProperty.getCounter("prayers").incrementAndGet();
    hero2AlignmentProperty.setAlignment(Alignment.GOOD, lineNumber);

    return Collections.emptyList();
  }
}
