package de.saviola.irpglp.modules.alignment.property;

import de.saviola.irpglp.property.CounterStoreProperty;

/**
 * {@link CounterStoreProperty} for the hero's alignment.
 * 
 * In addition to the hero's alignment, the following counters are stored:
 * 
 * - number of "prayers"
 * - number of times the hero was "forsaken" by his evil god
 * - number of items stolen by the hero
 * - number of items stolen from the hero
 */
public class AlignmentProperty extends CounterStoreProperty<AlignmentProperty>
{
  /**
   * Alignment enum.
   * 
   * Possible alignments: Neutral (N), good (G), evil (E).
   */
  public enum Alignment
  {
    NEUTRAL('N'), GOOD('G'), EVIL('E');

    /**
     * Alignment ID (N, G or E).
     */
    private final char id;

    private Alignment(final char id)
    {
      this.id = id;
    }

    @Override
    public String toString()
    {
      return String.valueOf(this.id);
    }
  }

  /**
   * The hero's alginment.
   */
  private Alignment alignment = Alignment.NEUTRAL;

  /**
   * Returns the hero's alignment.
   */
  public Alignment getAlignment()
  {
    return this.alignment;
  }

  @Override
  public String[] getValues()
  {
    return new String[] {
      this.alignment.toString(),
      String.format("%d", this.getCounter("prayers").get()),
      String.format("%d", this.getCounter("forsakens").get()),
      String.format("%d", this.getCounter("itemsStolenThief").get()),
      String.format("%d", this.getCounter("itemsStolenVictim").get()),
    };
  }

  /**
   * Sets the hero's alignment.
   */
  public synchronized void setAlignment(final Alignment alignment,
    final long lineNumber)
  {
    // Only update the alignment if it is the newest known alignment change
    if (this.getLineNumberTracker().checkAndUpdateLineNumber(lineNumber))
    {
      this.alignment = alignment;
    }
  }
}
