package de.saviola.irpglp.modules.alignment.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.alignment.property.AlignmentProperty;
import de.saviola.irpglp.modules.alignment.property.AlignmentProperty.Alignment;
import de.saviola.irpglp.modules.log.model.LogLine;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name> is forsaken by his evil god[...]"
 */
public class ForsakenProcessor extends PatternMatchingLogLineProcessor
{
  public ForsakenProcessor()
  {
    super("forsaken");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "forsaken", "(?<hero>$(name)) is forsaken by his evil god.*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    // Extract hero
    final List<Hero> heroes =
      this.extractHeroes(input, new String[] { "hero" });

    // Update property
    final AlignmentProperty property = (AlignmentProperty)
      this.getProperty(heroes.get(0), "alignment");
    property.getCounter("forsakens").incrementAndGet();
    property.setAlignment(Alignment.EVIL, ((LogLine) input).getLineNumber());

    return Collections.emptyList();
  }
}
