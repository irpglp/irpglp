package de.saviola.irpglp.modules.alignment.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link AlignmentProperty}.
 */
public class AlignmentDescriptor extends PropertyDescriptor<AlignmentProperty>
{
  public AlignmentDescriptor()
  {
    super("alignment");
  }

  @Override
  public AlignmentProperty createProperty()
  {
    return new AlignmentProperty();
  }

  @Override
  public String[] getColumns()
  {
    //@formatter:off
    return new String[] {
      "align", "5", "c",
        "alignment of the hero (G = good, N = neutral, E = evil)",
      "prayers", "0", "r",
        "number of prayers the hero has spoken (good only)",
      "forsaken", "0", "r",
        "number of times the hero has been forsaken by their evil god (evil only)",
      "thief", "0", "r",
        "number of times the hero has stolen a good hero's item (evil only)",
      "victim", "0", "r",
        "number of times the hero has fallen victim to the above (good only)",
    };
    //@formatter:on
  }
}
