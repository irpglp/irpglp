package de.saviola.irpglp.modules.fate.processor;

import java.util.Collection;
import java.util.Collections;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.fate.property.FateProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "Thereupon He stretched out His little finger among them and consumed
 * <name>[...]"
 */
public class FingerOfGodProcessor extends PatternMatchingLogLineProcessor
{
  public FingerOfGodProcessor()
  {
    super("fingerOfGod");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[]
    {
      "fingerOfGod", "Thereupon He stretched out His little finger among " +
        "them and consumed (?<hero>$(name)).*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final Hero hero = this.extractHeroes(input, new String[] { "hero" }).get(0);

    ((FateProperty) this.getProperty(hero, "fate")).getCounter("fingersOfGod")
      .incrementAndGet();

    return Collections.emptyList();
  }
}
