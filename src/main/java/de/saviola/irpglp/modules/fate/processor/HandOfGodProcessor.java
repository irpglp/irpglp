package de.saviola.irpglp.modules.fate.processor;

import java.util.Collection;
import java.util.Collections;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.fate.property.FateProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "Verily I say unto thee, the Heavens have burtst forth, and the blessed
 * hand of God carried <name>[...]"
 */
public class HandOfGodProcessor extends PatternMatchingLogLineProcessor
{
  public HandOfGodProcessor()
  {
    super("handOfGod");
  }

  @Override
  public String[] getRegularExpressions()
  {
    // TODO include level + time to next
    return new String[]
    {
      "handOfGod", "Verily I say unto thee, the Heavens have burst forth, " +
        "and the blessed hand of God carried (?<hero>$(name)).*"
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final Hero hero = this.extractHeroes(input, new String[] { "hero" }).get(0);

    ((FateProperty) this.getProperty(hero, "fate")).getCounter("handsOfGod")
      .incrementAndGet();

    return Collections.emptyList();
  }
}
