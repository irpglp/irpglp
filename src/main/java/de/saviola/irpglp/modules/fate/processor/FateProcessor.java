package de.saviola.irpglp.modules.fate.processor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.fate.property.FateProperty;
import de.saviola.irpglp.modules.level.property.LevelProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name> [...]!/. This terrible/wondrous calamity/godsend has
 * slowed/accelerated them <time> from/towards level <level>"
 */
public class FateProcessor extends PatternMatchingLogLineProcessor
{
  public FateProcessor()
  {
    super("fate");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[] {
      "fateTypePrefix", "terrible|wondrous",
      "fateType", "calamity|godsend",
      "fateTypeSuffix", "slowed|accelerated",
      "fate", "(?<name>$(name)) .*(?:!|\\.) This (?:$(fateTypePrefix)) " +
        "(?<fateType>$(fateType)) has (?:$(fateTypeSuffix)) them " +
        "$(idleTime) (?:from|towards) level (?<level>\\d+)",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final List<String> groups = this.getNamedGroups(input.toString(),
      new String[] { "fateType", "level" });

    final Hero hero = this.extractHeroes(input, new String[] { "name" }).get(0);

    final FateProperty fate = (FateProperty) this.getProperty(hero, "fate");

    // Increment the appropriate counter
    if (groups.get(0).equals("calamity"))
    {
      fate.getCounter("calamities").incrementAndGet();
    }
    else
    {
      fate.getCounter("godsends").incrementAndGet();
    }

    // Update the hero's level
    final LevelProperty level =
      (LevelProperty) this.getProperty(hero, "level");

    level.setLevel(Integer.valueOf(groups.get(1)).intValue() - 1);

    return Collections.emptyList();
  }
}
