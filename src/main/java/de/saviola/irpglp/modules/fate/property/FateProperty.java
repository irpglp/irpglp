package de.saviola.irpglp.modules.fate.property;

import de.saviola.irpglp.property.CounterStoreProperty;

/**
 * {@link CounterStoreProperty} for the fateful encounters of the hero.
 * 
 * Stored counter are:
 * 
 * - number of "terrible calamities"
 * - number of "godsends"
 * - number of "hands of god"
 * - number of "fingers of god"
 */
public class FateProperty extends CounterStoreProperty<FateProperty>
{
  @Override
  public String[] getValues()
  {
    return new String[]
    {
      String.format("%d", this.getCounter("calamities").get()),
      String.format("%d", this.getCounter("godsends").get()),
      String.format("%d", this.getCounter("handsOfGod").get()),
      String.format("%d", this.getCounter("fingersOfGod").get()),
    };
  }
}
