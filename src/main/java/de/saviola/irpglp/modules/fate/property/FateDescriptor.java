package de.saviola.irpglp.modules.fate.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link FateProperty}.
 */
public class FateDescriptor extends PropertyDescriptor<FateProperty>
{
  public FateDescriptor()
  {
    super("fate");
  }

  @Override
  public FateProperty createProperty()
  {
    return new FateProperty();
  }

  @Override
  public String[] getColumns()
  {
    return new String[] {
      "calamities", "0", "r",
      "number of \"terrible calamities\" the hero has had",
      "godsends", "0", "r",
      "number of \"wondrous godsends\" the hero has had",
      "HoG", "0", "r",
      "number of times the \"heavens have burst forth\" for the hero",
      "FoG", "0", "r",
      "number of times the hero was \"consumed with fire\"",
    };
  }

}
