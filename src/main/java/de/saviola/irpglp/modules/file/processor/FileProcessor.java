package de.saviola.irpglp.modules.file.processor;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.io.Files;

import de.saviola.irpglp.processor.IrpglpProcessor;

/**
 * Processes log files and returns a list of Strings containing the lines.
 * 
 * The line order in the list will be reversed for no other reason than to
 * allow more efficient processing in scenarios where later lines may make
 * information from earlier lines irrelevant. 
 * 
 * Lines will be prefixed with "LC#<lineNumber>#" to allow line number tracking
 * 
 * TODO global line numbering when processing multiple files
 */
public class FileProcessor extends IrpglpProcessor
{

  @Override
  public boolean isApplicableFor(final Object input)
  {
    return input instanceof File;
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    // Read files from line and reverse them.
    // Why reverse? Newest stuff shall be processed first.
    final List<String> lines = Lists.reverse(Files.readLines((File) input,
      Charset.forName("utf-8")));

    final int lineCount = lines.size();
    for (int i = 0; i < lineCount; i++)
    {
      lines.set(i, "LC#" + (lineCount - i) + "# " + lines.get(i));
    }

    return lines;
  }
}
