package de.saviola.irpglp.modules.name.property;

import de.saviola.irpglp.property.Property;

/**
 * {@link Property} for the hero's name.
 */
public class NameProperty extends Property<NameProperty>
{
  /**
   * The hero's name.
   */
  private String name;

  /**
   * Returns the hero's name.
   */
  public String getName()
  {
    return this.name;
  }

  @Override
  public String[] getValues()
  {
    return new String[] { this.getName() };
  }

  /**
   * Sets the hero's name.
   */
  public synchronized void setName(final String name)
  {
    this.name = name;
  }

  @Override
  public String toString()
  {
    return this.name;
  }
}
