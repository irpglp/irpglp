package de.saviola.irpglp.modules.name.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link NameProperty}.
 */
public class NameDescriptor extends PropertyDescriptor<NameProperty>
{
  public NameDescriptor()
  {
    super("name");
  }

  @Override
  public NameProperty createProperty()
  {
    return new NameProperty();
  }

  @Override
  public String[] getColumns()
  {
    return new String[] { "name", "20", "l", "name of the hero" };
  }
}
