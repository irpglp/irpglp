package de.saviola.irpglp.modules.meta.processor;

import java.util.Collection;
import java.util.Collections;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.log.model.LogLine;
import de.saviola.irpglp.modules.meta.property.MetaProperty;
import de.saviola.irpglp.processor.PatternMatchingLogLineProcessor;

/**
 * Pattern machting processor.
 * 
 * Format of the matched lines:
 * 
 * "<name>, the level [...], is now online from nickname <username>.[...]"
 */
public class LoginProcessor extends PatternMatchingLogLineProcessor
{
  public LoginProcessor()
  {
    super("login");
  }

  @Override
  public String[] getRegularExpressions()
  {
    return new String[]
    {
      // TODO extract time to next level
      "login", "(?<hero>$(name)), the level .*, is now online from nickname " +
        "(?<user>$(name))\\..*",
    };
  }

  @Override
  public Collection<?> process(final Object input) throws Exception
  {
    super.process(input);

    final Hero hero = this.extractHero(input);
    final String username =
      this.getNamedGroups(input, new String[] { "user" }).get(0);

    final MetaProperty property = (MetaProperty) this.getProperty(hero, "meta");
    // Increment number of logins and set the username
    property.getCounter("logins").incrementAndGet();
    property.setUsername(username, ((LogLine) input).getLineNumber());

    return Collections.emptyList();
  }
}
