package de.saviola.irpglp.modules.meta.property;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * {@link PropertyDescriptor} for the {@link MetaProperty}.
 */
public class MetaDescriptor extends PropertyDescriptor<MetaProperty>
{
  public MetaDescriptor()
  {
    super("meta");
  }

  @Override
  public MetaProperty createProperty()
  {
    return new MetaProperty();
  }

  @Override
  public String[] getColumns()
  {
    return new String[]
    {
      "logins", "0", "r", "number of times the has player has logged in",
      "username", "15", "l",
      "username of the last player who has logged in as the hero"
    };
  }

}
