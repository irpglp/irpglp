package de.saviola.irpglp.modules.meta.property;

import de.saviola.irpglp.property.CounterStoreProperty;

/**
 * {@link CounterStoreProperty} for meta information.
 *
 * Currently, only the username from which the hero has logged in most
 * recently is stored.
 *
 * TODO naming? UsernameProperty?
 */
public class MetaProperty extends CounterStoreProperty<MetaProperty>
{
  /**
   * Username of the player behind the hero.
   */
  private String username;

  /**
   * Returns the username.
   */
  public String getUsername()
  {
    return this.username;
  }

  @Override
  public String[] getValues()
  {
    String username = this.getUsername() != null ? this.getUsername() : " ";
    username = username.length() <= 15 ? username
      : username.substring(0, 14) + "…";

    return new String[]
    {
      String.format("%d", this.getCounter("logins").get()),
      username
    };
  }

  /**
   * Sets the username.
   *
   * TODO check the line?
   */
  public synchronized void setUsername(final String username,
    final long lineNumber)
  {
    if (this.getLineNumberTracker().checkAndUpdateLineNumber(lineNumber))
    {
      this.username = username;
    }
  }

}
