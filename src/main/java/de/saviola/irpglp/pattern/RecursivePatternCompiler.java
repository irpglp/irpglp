package de.saviola.irpglp.pattern;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of a recursive pattern compiler.
 * 
 * The patterns are passed to the constructor as a map of IDs and associated
 * pattern string. These have to be compiled. Before compiling the patterns,
 * however, they are parsed and variables referencing other patterns are
 * recursively replaced. IDs may only contain characters and numbers.
 * 
 * Example of IDs and pattern strings:
 * 
 * "id1", "Pattern example($(id2))"
 * "id2", "!|."
 * 
 * TODO Protection from endless recursion.
 * TODO better naming?
 * TODO pattern ID instead of name
 */
public class RecursivePatternCompiler implements Callable<Map<String, Pattern>>
{
  private final static Logger logger = LoggerFactory
    .getLogger(RecursivePatternCompiler.class);

  /**
   * Pattern for variables within patterns (which are, in turn, IDs of other
   * patterns).
   */
  private final Pattern variablePattern = Pattern
    .compile("(\\$\\(([a-zA-Z0-9]+)\\))");

  /**
   * Map of pattern strings.
   * 
   * Will be passed to the constructor.
   */
  private final Map<String, String> patternStrings;

  /**
   * Map of compiled patterns.
   */
  private final Map<String, Pattern> patterns = new HashMap<>();

  public RecursivePatternCompiler(final Map<String, String> patternStrings)
  {
    this.patternStrings = patternStrings;
  }

  /**
   * Parses and compiles all patern string and return the compiled patterns.
   */
  @Override
  public Map<String, Pattern> call() throws Exception
  {
    for (final String patternName : this.patternStrings.keySet())
    {
      this.parseAndCompile(patternName);
    }

    return this.patterns;
  }

  /**
   * Compiles the pattern for the given name and stores in the pattern map.
   */
  private Pattern compile(final String name)
  {
    logger.debug("Pattern {} stored: {}", name, this.patternStrings.get(name));

    return this.patterns.put(name,
      Pattern.compile(this.patternStrings.get(name)));
  }

  /**
   * Recursively parses the pattern for the given name.
   * 
   * TODO don't call parseAndCompile within this method?
   */
  private String parse(final String name)
  {
    String patternString = this.patternStrings.get(name);

    // If no pattern exists for the given name
    if (patternString == null)
    {
      throw new RuntimeException("Unkown subpattern: " + name);
    }

    // Matcher for variables in the pattern string
    final Matcher matcher = this.variablePattern.matcher(patternString);

    // As long as there are still variables left
    while (matcher.find())
    {
      // Parse and compile the pattern for the variable and replace the
      // variable with the pattern string
      patternString =
        matcher.replaceFirst(StringEscapeUtils.escapeJava(this
          .parseAndCompile(matcher.group(2))));

      matcher.reset(patternString);
    }

    return patternString;
  }

  /**
   * Parses and compiles the pattern for the given name.
   * 
   * Returns the pattern string which is used in {@link #parse(String)}.
   * 
   * TODO make the algorithm more clean and clear.
   */
  private String parseAndCompile(final String name)
  {
    // If the pattern had already been compiled, just return the string for it
    if (this.patterns.containsKey(name))
    {
      return this.patternStrings.get(name);
    }

    // Otherwise parse the pattern and overwrite the unparsed version
    this.patternStrings.put(name, this.parse(name));

    // Compile pattern
    this.compile(name);

    return this.patternStrings.get(name);
  }
}
