package de.saviola.irpglp.model;

/**
 * Class that allows line number tracking.
 * 
 * When parallely processing lines from a log file, the original order of the
 * lines is lost. This class can be used, for example, to make sure that
 * information from line A does not overwrite information from
 * line B, if A is before B in the log file, but B is processed before A. 
 */
public class LineNumberTracker
{
  /**
   * The highest known line number.
   */
  private long lineNumber = 0;

  /**
   * Checks whether the given line number is greater then the higest known line
   * number and returns true if it is, false otherwise.
   * 
   * In the former case, the new line number is stored.
   */
  public synchronized boolean checkAndUpdateLineNumber(final long lineNumber)
  {
    if (lineNumber > this.lineNumber)
    {
      this.setLineNumber(lineNumber);

      return true;
    }

    return false;
  }

  /**
   * Returns the highest known line number.
   */
  public synchronized long getLineNumber()
  {
    return this.lineNumber;
  }

  /**
   * Sets the line number.
   * 
   * @see LineNumberTracker#checkAndUpdateLineNumber(long)
   */
  private void setLineNumber(final long lineNumber)
  {
    this.lineNumber = lineNumber;
  }
}
