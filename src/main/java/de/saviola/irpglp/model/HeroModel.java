package de.saviola.irpglp.model;

import java.util.List;

import de.saviola.irpglp.Hero;

/**
 * Interface for model classes involving heroes.
 */
public interface HeroModel extends Model
{
  /**
   * Returns a list of involved heroes.
   */
  public List<Hero> getInvolvedHeroes();
}
