package de.saviola.irpglp.reflect;

import java.lang.reflect.Modifier;

import de.saviola.regep.core.filter.interfaces.Filter;

/**
 * Filter for abstract classes.
 */
public class AbstractClassFilter implements Filter
{
  /**
   * Returns true if the given object is an abstract class, false otherwise.
   */
  @Override
  public boolean accept(final Object input)
  {
    return !Modifier.isAbstract(((Class<?>) input).getModifiers());
  }

  /**
   * Returns true of the given object is a {@link Class} object.
   */
  @Override
  public boolean isApplicableFor(final Object input)
  {
    return input instanceof Class;
  }

}
