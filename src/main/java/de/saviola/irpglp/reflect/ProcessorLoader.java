package de.saviola.irpglp.reflect;

import java.util.Collection;
import java.util.concurrent.Callable;

import de.saviola.irpglp.processor.IrpglpProcessor;

/**
 * Loads and instanciates all {@link IrpglpProcessor} classes.
 */
public class ProcessorLoader implements
  Callable<Collection<IrpglpProcessor>>
{
  @Override
  public Collection<IrpglpProcessor> call() throws Exception
  {
    // Load and initialize processors
    return new ClassInstantiator<>(
      new ClassLoader<>(IrpglpProcessor.class).call()).call();
  }
}
