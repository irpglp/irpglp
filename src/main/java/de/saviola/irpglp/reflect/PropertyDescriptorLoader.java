package de.saviola.irpglp.reflect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

import de.saviola.irpglp.property.PropertyDescriptor;

/**
 * Loads and returns all property descriptors.
 */
public class PropertyDescriptorLoader implements
  Callable<Collection<PropertyDescriptor<?>>>
{
  /**
   * Collection of property descriptors.
   */
  private final Collection<PropertyDescriptor<?>> descriptors =
    new ArrayList<>();

  /**
   * Loads and returns all property descriptors.
   */
  @Override
  public Collection<PropertyDescriptor<?>> call() throws Exception
  {
    this.initializePropertyDescriptors();

    return this.descriptors;
  }

  /**
   * Initializes the property descriptors by instantiating them
   */
  private void initializePropertyDescriptors() throws Exception
  {
    for (final PropertyDescriptor<?> descriptor : new ClassInstantiator<>(
      new ClassLoader<>(PropertyDescriptor.class).call()).call())
    {
      this.descriptors.add(descriptor);
    }
  }
}
