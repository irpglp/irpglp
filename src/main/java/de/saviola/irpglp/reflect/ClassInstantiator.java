package de.saviola.irpglp.reflect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

import de.saviola.regep.core.filter.interfaces.Filter;
import de.saviola.regep.core.filter.interfaces.FilterSupport;

/**
 * A simple class instantiator which takes a {@link Collection} of
 * {@link Class} objects and tries to create instances for each of them.
 * 
 * @param <V> Highest type boundary for classes accepted by this instantiator
 * 
 * TODO make sure that no abstract classes are instantiated even without the
 * filter
 */
public class ClassInstantiator<V> implements Callable<Collection<V>>,
  FilterSupport
{
  /**
   * Filter for classes.
   * 
   * Defaults to an {@link AbstractClassFilter} to avoid attempting to
   * instantiate abstract classes.
   */
  private Filter filter = new AbstractClassFilter();

  /**
   * Classes to be instantiated.
   */
  private Collection<Class<? extends V>> classes;

  /**
   * Creates a new instantiator for the given classes.
   */
  public ClassInstantiator(final Collection<Class<? extends V>> classes)
  {
    this.classes = classes;
  }

  /**
   * Creates a new instantiator for the given classes, using the given filter.
   */
  public ClassInstantiator(final Collection<Class<? extends V>> classes,
    final Filter filter)
  {
    this(classes);

    this.setFilter(filter);
  }

  /**
   * Returns a collection of all objects that were created.
   */
  @Override
  public Collection<V> call() throws Exception
  {
    final Collection<V> objects = new ArrayList<>();

    V currentObject;

    // Walk through all classes
    for (final Class<? extends V> currentClass : this.classes)
    {
      // Skip class if our filter doesn't accpet it
      if (this.filter.isApplicableFor(currentClass)
        && !this.filter.accept(currentClass))
      {
        continue;
      }

      // Try to instantiate the class and add it to our set
      try
      {
        currentObject = currentClass.newInstance();

        objects.add(currentObject);
      }
      // TODO handle exceptions
      catch (final InstantiationException e)
      {
        e.printStackTrace();
      }
      catch (final IllegalAccessException e)
      {
        e.printStackTrace();
      }
    }

    return objects;
  }

  /**
   * Returns the filter used by this instantiator.
   */
  @Override
  public Filter getFilter()
  {
    return this.filter;
  }

  /**
   * Sets the filter to be used by this instantiator.
   */
  @Override
  public void setFilter(final Filter filter)
  {
    this.filter = filter;
  }
}
