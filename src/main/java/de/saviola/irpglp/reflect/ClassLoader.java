package de.saviola.irpglp.reflect;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.Callable;

import org.reflections.Reflections;

/**
 * Loads all classes within the package de.saviola.irpglp that are subtypes of
 * a certain super class.
 * 
 * @param <V> Super class as passed to the constructor.
 */
public class ClassLoader<V> implements
  Callable<Collection<Class<? extends V>>>
{
  /**
   * The super class.
   * 
   * Only classes extending this class will be loaded.
   */
  private final Class<V> superClass;

  /**
   * Creates a new class load using the given super class.
   */
  public ClassLoader(final Class<V> superClass)
  {
    this.superClass = superClass;
  }

  /**
   * Returns all loaded classes.
   */
  @Override
  public Collection<Class<? extends V>> call() throws Exception
  {
    return this.loadClasses();
  }

  /**
   * Loads the classes.
   * 
   * TODO make package another constructor argument?
   */
  private Set<Class<? extends V>> loadClasses()
  {
    final Reflections reflections = new Reflections("de.saviola.irpglp");

    return reflections.getSubTypesOf(this.superClass);
  }

}
