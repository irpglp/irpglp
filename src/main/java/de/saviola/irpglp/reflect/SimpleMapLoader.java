package de.saviola.irpglp.reflect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

import de.saviola.irpglp.storage.IrpglpStore;
import de.saviola.irpglp.storage.interfaces.GlobalStore;
import de.saviola.irpglp.storage.interfaces.SimpleStore;
import de.saviola.regep.core.filter.BlacklistFilter;
import de.saviola.regep.core.filter.ListFilterChain;
import de.saviola.regep.core.filter.interfaces.FilterChain;

/**
 * Loader for all {@link SimpleStore}s.
 */
public class SimpleMapLoader implements Callable<Collection<SimpleStore<?>>>
{
  /**
   * Loads and returns all stores.
   */
  @Override
  public Collection<SimpleStore<?>> call() throws Exception
  {
    // Set up a filter chain so that abstract classes and our main store
    // are not instantiated (again).
    final FilterChain filterChain = new ListFilterChain();
    filterChain.addFilter(new AbstractClassFilter());
    filterChain.addFilter(new BlacklistFilter<>(
      new Class<?>[] { IrpglpStore.class }));

    final Collection<SimpleStore<?>> stores = new ArrayList<>();

    for (final SimpleStore<?> store : new ClassInstantiator<>(new ClassLoader<>(
      GlobalStore.class).call(), filterChain).call())
    {
      stores.add(store);
    }

    return stores;
  }
}
