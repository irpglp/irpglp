package de.saviola.irpglp.output;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.brsanthu.dataexporter.DataExporter;
import com.brsanthu.dataexporter.model.AlignType;
import com.brsanthu.dataexporter.model.StringColumn;
import com.brsanthu.dataexporter.output.texttable.TextTableExportOptions;
import com.brsanthu.dataexporter.output.texttable.TextTableExporter;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.property.PropertyDescriptor;
import de.saviola.irpglp.storage.HeroStore;
import de.saviola.irpglp.storage.IrpglpStore;
import de.saviola.irpglp.storage.PropertyDescriptorStore;

/**
 * Class which creates the text table output from the data.
 */
public class TextTableOutput
{
  /**
   * Properties to be collected in the output.
   * 
   * String representations of the properties, passed to the constructor.
   */
  private final String[] properties;

  /**
   * Main storage, to provide access to property store etc.
   */
  private final IrpglpStore store;

  /**
   * Data exporter that will be used to gather data and write the output.
   */
  private DataExporter exporter;

  public TextTableOutput(final IrpglpStore database, final String[] properties)
  {
    this.properties = properties;
    this.store = database;
  }

  /**
   * Creates the table columns.
   * 
   * @see PropertyDescriptor#getColumns()
   */
  private void createColumns()
  {
    final PropertyDescriptorStore propertyDescriptorStore =
      (PropertyDescriptorStore) this.store.get("propertyDescriptor");
    String[] rawColumnData;
    StringColumn column;
    int columnWidth;

    // Walk through all properties
    for (int i = 0; i < this.properties.length; i++)
    {
      // Get the columns for the property
      rawColumnData = propertyDescriptorStore.get(this.properties[i])
        .getColumns();

      // Walk through the columns
      for (int j = 0; j < rawColumnData.length; j += 4)
      {
        // Column heading
        column = new StringColumn(rawColumnData[j]);

        // Column width
        columnWidth = Integer.valueOf(rawColumnData[j + 1]).intValue();

        // Column width of 0 means automatic, otherwise set the width
        if (columnWidth > 0)
        {
          column.setWidth(columnWidth);
        }

        // Column alignment
        switch (rawColumnData[j + 2].charAt(0))
        {
          case 'l':
            column.setAlign(AlignType.TOP_LEFT);
          break;
          case 'c':
            column.setAlign(AlignType.TOP_CENTER);
          break;
          case 'r':
            column.setAlign(AlignType.TOP_RIGHT);
          break;
          default:
            throw new RuntimeException("Unknown alignment identifier.");
        }

        // Add the column in the exporter
        this.exporter.addColumns(column);
      }
    }
  }

  /**
   * Creates and writes the output to the given file.
   * 
   * TODO extract creation or rename?
   */
  public void write(final File file)
    throws IOException
  {
    final FileWriter fw = new FileWriter(file);

    // Fetch the needed stores
    final PropertyDescriptorStore propertyDescriptorStore =
      (PropertyDescriptorStore) this.store.get("propertyDescriptor");
    final HeroStore heroStore = (HeroStore) this.store.get("hero");

    // Get property descriptors
    final Collection<PropertyDescriptor<?>> propertyDescriptors =
      propertyDescriptorStore.getAll();

    // Create the exporter using the above file writer
    this.exporter = new TextTableExporter(fw);
    // The header is repeated after every 10 lines
    ((TextTableExportOptions) this.exporter.getOptions())
      .setRepeatHeadersAfterRows(10);

    // Create the columns
    this.createColumns();

    // Start exporting, no columns can be added anymore
    this.exporter.startExporting();

    // Retrieve the heroes and sort them
    final List<Hero> heroes = new ArrayList<>(heroStore.getAll());
    Collections
      .sort(heroes, new DefaultHeroComparator(propertyDescriptorStore));

    // Loop variables
    final List<Object> rowValues = new ArrayList<>(propertyDescriptors.size());
    String[] rawValues;

    // Walk through the heroes
    for (final Hero hero : heroes)
    {
      rowValues.clear();

      // Walk through the properties
      for (int i = 0; i < this.properties.length; i++)
      {
        // Get the values for he property
        rawValues = hero.getProperty(propertyDescriptorStore
          .get(this.properties[i])).getValues();

        // And walk through all the collected values and add them to the row
        for (int j = 0; j < rawValues.length; j++)
        {
          rowValues.add(rawValues[j]);
        }
      }

      // Finally, add the row in the exporter
      this.exporter.addRow(rowValues.toArray());
    }

    // Finish exporting
    this.exporter.finishExporting();

    // Flush and close the file writer
    // TODO needed?
    fw.flush();
    fw.close();
  }

  /**
   * Creates and writes the column descriptions.
   * 
   * TODO extract creation or rename
   */
  public void writeColumnDescriptions(final File file) throws IOException
  {
    // Create file writer and data exporter
    final FileWriter fw = new FileWriter(file);
    this.exporter = new TextTableExporter(fw);

    // Get the property descriptor store and all property descriptor
    final PropertyDescriptorStore propertyDescriptorStore =
      (PropertyDescriptorStore) this.store.get("propertyDescriptor");
    final Collection<PropertyDescriptor<?>> propertyDescriptors =
      propertyDescriptorStore.getAll();

    // Add the two colums: One for the field, one for the description
    this.exporter.addColumns(new StringColumn("Field", 10, AlignType.TOP_LEFT),
      new StringColumn("Module", 10), new StringColumn("Description", 100,
        AlignType.TOP_LEFT));

    this.exporter.startExporting();

    String[] columns;

    // Walk through all property descriptors
    for (final PropertyDescriptor<?> propertyDescriptor : propertyDescriptors)
    {
      // Get the columns for the property descriptor
      columns = propertyDescriptor.getColumns();

      // Walk through the columns
      for (int i = 0; i < columns.length; i += 4)
      {
        // Add a row with the column and its description
        this.exporter.addRow(columns[i], propertyDescriptor.getId(),
          columns[i + 3]);
      }
    }

    this.exporter.finishExporting();

    // Flush and close the file writer
    fw.flush();
    fw.close();
  }
}