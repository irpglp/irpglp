package de.saviola.irpglp.output;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;

/**
 * Compiles the output from multiple files into one file.
 * 
 * TODO make the class more flexible
 */
public class TextFileOutputCompiler
{
  /**
   * Output file.
   * 
   * Is passed to the constructor.
   */
  private final File outputFile;

  public TextFileOutputCompiler(final File outputFile)
  {
    this.outputFile = outputFile;
  }

  /**
   * Compile and save the output file(s).
   */
  public void compileAndSave() throws IOException
  {
    // Header file
    final String header = FileUtils
      .readFileToString(new File("./templates/header.txt"));

    // Actual table with data
    final String table = FileUtils
      .readFileToString(new File("/tmp/idle/table.txt"));

    // Description of the table
    final String tableDescription = FileUtils
      .readFileToString(new File("/tmp/idle/table_description.txt"));

    // Footer file
    final String footer = FileUtils
      .readFileToString(new File("./templates/footer.txt"));

    // Timestamp
    final String timestamp = "Timestamp: " + new Date().toString();

    FileUtils.write(this.outputFile, header + table + tableDescription + footer
      + timestamp);
  }
}
