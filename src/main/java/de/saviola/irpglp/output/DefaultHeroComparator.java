package de.saviola.irpglp.output;

import java.util.Comparator;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.level.property.LevelDescriptor;
import de.saviola.irpglp.modules.level.property.LevelProperty;
import de.saviola.irpglp.storage.PropertyDescriptorStore;

/**
 * Comparator for hero objects.
 */
public class DefaultHeroComparator implements Comparator<Hero>
{
  /**
   * Property descriptor for the level property.
   */
  private final LevelDescriptor levelDescriptor;

  public DefaultHeroComparator(
    final PropertyDescriptorStore propertyDescriptorStore)
  {
    // Get the level descriptor from the descriptor store
    this.levelDescriptor =
      (LevelDescriptor) propertyDescriptorStore.get("level");
  }

  /**
   * Compares the given heroes.
   * 
   * Heroes are compared by level and time to next level.
   */
  @Override
  public int compare(final Hero h1, final Hero h2)
  {
    final LevelProperty l1 =
      (LevelProperty) h1.getProperty(this.levelDescriptor);
    final LevelProperty l2 =
      (LevelProperty) h2.getProperty(this.levelDescriptor);

    if (l1.getLevel() != l2.getLevel())
    {
      return l2.getLevel() - l1.getLevel();
    }

    return new Long(l1.getSecondsToNext() - l2.getSecondsToNext()).intValue();
  }
}
