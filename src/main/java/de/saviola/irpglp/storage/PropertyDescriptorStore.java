package de.saviola.irpglp.storage;

import de.saviola.irpglp.property.PropertyDescriptor;
import de.saviola.irpglp.reflect.PropertyDescriptorLoader;
import de.saviola.irpglp.storage.interfaces.GlobalStore;

/**
 * Store of {@link PropertyDescriptor}s.
 */
public class PropertyDescriptorStore
  extends DefaultStore<PropertyDescriptor<?>>
  implements GlobalStore<PropertyDescriptor<?>>
{
  @Override
  public String getId()
  {
    return "propertyDescriptor";
  }

  /**
   * Loads all property descriptors upon initialization.
   */
  @Override
  public void initialize() throws Exception
  {
    for (final PropertyDescriptor<?> descriptor : new PropertyDescriptorLoader()
      .call())
    {
      this.put(descriptor);
    }

    super.initialize();
  }
}
