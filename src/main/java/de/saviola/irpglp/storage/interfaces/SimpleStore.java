package de.saviola.irpglp.storage.interfaces;

import java.util.Collection;
import java.util.Map;

/**
 * Simple interface wrapper for a {@link Map} of string keys and arbitrary
 * values.
 * 
 * @param <V> Type of the stored values.
 * 
 * TODO better naming
 */
public interface SimpleStore<V>
{
  /**
   * Returns true if the store contains an item for the given key.
   * 
   * @see Map#containsKey(Object)
   */
  public boolean contains(String key);

  /**
   * Implementation of a (default) value generation for a given key.
   */
  public V createEntry(String key);

  /**
   * Returns the stored value for the given key.
   * 
   * @see Map#get(Object)
   */
  public V get(String key);

  /**
   * Returns a collection of all values.
   * 
   * @see Map#values()
   * TODO rename?
   */
  public Collection<V> getAll();

  /**
   * Returns the ID of this store.
   */
  public String getId();

  /**
   * Returns the interal map of keys and values.
   */
  public Map<String, V> getMap();

  /**
   * Returns the parent store or null if this store has no parent store.
   */
  public SimpleStore<SimpleStore<?>> getParentStore();

  /**
   * Initialization hook.
   */
  public void initialize() throws Exception;

  /**
   * Inserts a value for the given key.
   * 
   * @see Map#put(Object, Object)
   */
  public V put(String key, V value);

  /**
   * Inserts a value, extracting its key in some way (e.g. by using
   * {@link #toString()}).
   */
  public V put(V value);

  /**
   * Inserts all given values into the store using {@link #put(Object)}.
   * 
   * @see Map#putAll(Map)
   * TODO implement for {@link #put(String, Object)}?
   */
  public void putAll(Collection<V> values);

  /**
   * Sets the interal map of this store.
   */
  public void setMap(Map<String, V> map);

  /**
   * Sets the parent store.
   */
  public void setParentStore(SimpleStore<SimpleStore<?>> parentStore);
}
