package de.saviola.irpglp.storage.interfaces;

/**
 * Tagging interface for singleton stores (or global stores).
 */
public interface GlobalStore<V> extends SimpleStore<V>
{

}
