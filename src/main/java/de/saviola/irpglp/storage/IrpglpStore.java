package de.saviola.irpglp.storage;

import de.saviola.irpglp.reflect.SimpleMapLoader;
import de.saviola.irpglp.storage.interfaces.SimpleStore;

/**
 * Main store in which all other stores are stored. :D
 */
public class IrpglpStore extends DefaultStore<SimpleStore<?>>
{
  /**
   * Loads all other stores upon initialization.
   */
  @Override
  public void initialize() throws Exception
  {
    for (final SimpleStore<?> map : new SimpleMapLoader().call())
    {
      map.setParentStore(this);
      map.initialize();
      this.put(map.getId(), map);
    }
  }
}
