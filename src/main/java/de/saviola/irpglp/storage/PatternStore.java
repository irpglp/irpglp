package de.saviola.irpglp.storage;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import de.saviola.irpglp.pattern.RecursivePatternCompiler;
import de.saviola.irpglp.processor.IrpglpProcessor;
import de.saviola.irpglp.processor.PatternMatchingStringProcessor;
import de.saviola.irpglp.storage.interfaces.GlobalStore;

/**
 * Store of patterns.
 */
public class PatternStore extends DefaultStore<Pattern> implements
  GlobalStore<Pattern>
{
  /**
   * Whether patterns were already loaded and compiled
   * 
   * TODO rename
   */
  private boolean compiled = false;

  /**
   * Loads and compiles patterns.
   * 
   * Can't be done in the {@link #initialize()} method because the processor
   * store has to be initialized before.
   * 
   * TODO rename or change, patterns are loaded and compiled
   * TODO store initialization dependency management?
   */
  public void compilePatterns()
  {
    final ProcessorStore processorStore =
      (ProcessorStore) this.getParentStore().get("processor");

    final Map<String, String> regularExpressions = new HashMap<>();
    String[] rawRegularExpressions;

    // Walks through all processors
    for (final IrpglpProcessor processor : processorStore.getAll())
    {
      // Skip the processor if it's no pattern matching processor
      if (!(processor instanceof PatternMatchingStringProcessor))
      {
        continue;
      }

      // Get the regular expressions (as strings) from the processor
      rawRegularExpressions = ((PatternMatchingStringProcessor) processor)
        .getRegularExpressions();

      // Put the regular expressions into a map
      for (int i = 0; i < rawRegularExpressions.length; i += 2)
      {
        regularExpressions.put(rawRegularExpressions[i],
          rawRegularExpressions[i + 1]);
      }
    }

    // Compile the patterns
    final RecursivePatternCompiler patternCompiler =
      new RecursivePatternCompiler(regularExpressions);

    try
    {
      this.setMap(patternCompiler.call());

      this.compiled = true;
    }
    catch (final Exception e)
    {
      // TODO Handle failure to compile patterns
      e.printStackTrace();
    }
  }

  @Override
  public Pattern get(final String name)
  {
    if (!this.compiled)
    {
      this.compilePatterns();
    }

    return super.get(name);
  }

  @Override
  public String getId()
  {
    return "pattern";
  }
}
