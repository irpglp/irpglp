package de.saviola.irpglp.storage;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * A store of {@link AtomicInteger} counters.
 */
public class CounterStore extends DefaultStore<AtomicInteger>
{
  @Override
  public AtomicInteger createEntry(final String key)
  {
    return new AtomicInteger();
  }

}
