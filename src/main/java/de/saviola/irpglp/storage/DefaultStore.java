package de.saviola.irpglp.storage;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.irpglp.storage.interfaces.SimpleStore;

/**
 * Default store implementation.
 */
abstract public class DefaultStore<V> implements SimpleStore<V>
{
  private final static Logger logger = LoggerFactory
    .getLogger(DefaultStore.class);

  /**
   * Data is stored in a {@link Map}.
   */
  private Map<String, V> data = new HashMap<>();

  /**
   * A potential parent store.
   */
  private SimpleStore<SimpleStore<?>> parentStore;

  @Override
  public boolean contains(final String key)
  {
    return this.data.containsKey(key);
  }

  @Override
  public V createEntry(final String key)
  {
    throw new UnsupportedOperationException();
  }

  /**
   * Synchronized helper method to create entries on demand.
   * 
   * TODO make on demand entry creation configurable
   */
  private synchronized void createEntryOnDemand(final String key)
  {
    if (!this.contains(key))
    {
      this.put(key, this.createEntry(key));
    }
  }

  @Override
  public V get(final String key)
  {
    this.createEntryOnDemand(key);

    return this.data.get(key);
  }

  @Override
  public Collection<V> getAll()
  {
    return this.data.values();
  }

  @Override
  public String getId()
  {
    throw new UnsupportedOperationException();
  }

  @Override
  public Map<String, V> getMap()
  {
    return this.data;
  }

  @Override
  public SimpleStore<SimpleStore<?>> getParentStore()
  {
    return this.parentStore;
  }

  @Override
  public void initialize() throws Exception
  {
    logger.debug("Initiailized store: {}", this.getId());
  }

  @Override
  public V put(final String key, final V value)
  {
    return this.data.put(key, value);
  }

  @Override
  public V put(final V value)
  {
    return this.put(value.toString(), value);
  }

  @Override
  public void putAll(final Collection<V> values)
  {
    for (final V value : values)
    {
      this.put(value);
    }
  }

  @Override
  public void setMap(final Map<String, V> map)
  {
    this.data = map;
  }

  @Override
  public void setParentStore(final SimpleStore<SimpleStore<?>> parentMap)
  {
    this.parentStore = parentMap;
  }
}
