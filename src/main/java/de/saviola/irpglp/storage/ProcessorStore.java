package de.saviola.irpglp.storage;

import de.saviola.irpglp.processor.IrpglpProcessor;
import de.saviola.irpglp.reflect.ProcessorLoader;
import de.saviola.irpglp.storage.interfaces.GlobalStore;

/**
 * Store of {@link IrpglpStore}s.
 */
public class ProcessorStore extends DefaultStore<IrpglpProcessor>
  implements GlobalStore<IrpglpProcessor>
{
  @Override
  public String getId()
  {
    return "processor";
  }

  /**
   * Loads all processors upon initialization.
   */
  @Override
  public void initialize() throws Exception
  {
    for (final IrpglpProcessor processor : new ProcessorLoader().call())
    {
      processor.setStorage(this.getParentStore());
      this.put(processor);
    }

    super.initialize();
  }
}
