package de.saviola.irpglp.storage;

import de.saviola.irpglp.Hero;
import de.saviola.irpglp.modules.name.property.NameProperty;
import de.saviola.irpglp.property.PropertyDescriptor;
import de.saviola.irpglp.storage.interfaces.GlobalStore;

/**
 * Store for hero objects.
 */
public class HeroStore extends DefaultStore<Hero> implements GlobalStore<Hero>
{
  /**
   * Creates a hero for the given key (which will be used as its name) if it
   * does not yet exist.
   */
  private synchronized void createHeroOnDemand(final String key)
  {
    // New heroes will be created on demand
    if (!this.contains(key))
    {
      // Create hero
      final Hero hero = new Hero();
      // Set hero name
      final NameProperty nameProperty =
        (NameProperty) hero.getProperty((PropertyDescriptor<?>)
          this.getParentStore().get("propertyDescriptor").get("name"));
      nameProperty.setName(key);

      // Store hero
      this.put(nameProperty.toString(), hero);
    }
  }

  /**
   * Returns the hero with the given name, creating it if necessary.
   * 
   * Use {@link #contains(String)} to check whether a hero existed before
   * calling this.
   */
  @Override
  public Hero get(final String key)
  {
    this.createHeroOnDemand(key);

    return super.get(key);
  }

  @Override
  public String getId()
  {
    return "hero";
  }
}
