The name column and column headings are repeated for better clarity.

Quests:
  When a player receives any kind of penalty while on a quest, the quests automatically and instantly fails (marked
  by the line "<name>'s prudence and self-regard[…]"), granting a penalty to _all_ heroes currently logged in. The
  "failed" column only counts the number of times the hero has caused a quest to fail, not the number of times they
  were involved in a quest that did not finish (the latter can be calculated by taking the number of started quests
  (column "quests") and subtracting the number of completed quests).

====================================================================================================================

Banner created using: http://patorjk.com/software/taag/
Text table output created using: https://github.com/brsanthu/data-exporter
Gitorious: https://gitorious.org/irpglp
Contact:  See the contact section on http://saviola.de or simply query me on IRC (preferred).
          Also, if you want (me) to create statistics for another IdleRGP instance, feel free to contact me.
Contribute: Apart from the obvious ways of contribution (i.e., contributing code, reporting bugs/typos etc.), I'm
            searching for fully timestamped logs reaching back as far as possible. Currently, the logs reach back as
            far as the beginning of 2011, so not very far. Also, my logs are neither complete (a maximum of 5% are
            missing due to buffer settings in my BNC), nor are they fully timestamped (for the same reason: BNC buffer
            playback).
            It would be awesome to create statistics using ALL events that ever happened on the EsperNet IdleRPG – this
            is just a first demonstration of the log parser using my own, limited logs.

Changelog:
  21.11.2013 – first public version
