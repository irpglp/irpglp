  _____    _ _     ____________ _____                   | This text file contains a couple of years worth of
 |_   _|  | | |    | ___ \ ___ \  __ \                  | statistics for the IdleRPG running on EsperNet. Since the
   | |  __| | | ___| |_/ / |_/ / |  \/                  | original statistics site has been dead for years, I
   | | / _` | |/ _ \    /|  __/| | __                   | decided to write a little log parser to aggregate the
  _| || (_| | |  __/ |\ \| |   | |_\ \                  | statistics below. Have fun and happy idling!
  \___/\__,_|_|\___\_| \_\_|    \____/                  |
           _____                    _   _        _      | For further information refer to the bottom of the file.
          |  ___|                  | \ | |      | |     |
          | |__ ___ _ __   ___ _ __|  \| |  ___ | |_    | Some links:
          |  __/ __| '_ \ / _ \ '__| . ` | / _ \| __|   |
          | |__\__ \ |_) |  __/ |  | |\  ||  __/| |_    | http://esper.net/ The IRC network we're idling in.
          \____/___/ .__/ \___|_|  \_| \_/ \___| \__|   | http://idlerpg.net/ The game we're playing.
                   | |                                  |
                   |_|                                  | View with line wrapping disabled! (In FF: <A-v>yb)
